document.addEventListener("DOMContentLoaded", function (event) {





    //Un commentaire


    // Bouton retour en haut de la page
    mybutton = document.querySelector("#myBtn");

    function topFunction() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera

    }

    mybutton.addEventListener('click', topFunction);

    //BD

    let connexion = new MovieDb();



    if (document.location.pathname.search("fiche-film.html") > 0){

        let params = ( new URL(document.location) ).searchParams;

        connexion.requestFilmInfos( params.get("id") );
        connexion.requestCastInfos( params.get("id") );

    }
    else{
        connexion.requestPopular();

        connexion.requestTopRated();
    }



});


class MovieDb {

    constructor() {

        this.APIKey = "b22f9b20c68ad36893d3c8b75f29771a";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "http://image.tmdb.org/t/p/";

        this.posterSizes = ["92", "154", "185", "342", "500", "780"];

        this.castSizes = ["45", "185"];

        this.movieTotal = 6;

        this.swiperTotal = 10;

        this.castTotal = 6;


    }

    requestTopRated(){

        var xhr = new XMLHttpRequest();
        // xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.returnRequestTopRated.bind(this));
        xhr.open("GET", this.baseURL +"movie/top_rated?page=1&language="+ this.lang +"&api_key="+ this.APIKey);
        xhr.send();
    }

    returnRequestTopRated(e){

        let target = e.currentTarget; //XMLHttpRequest
        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            this.afficheTopRated(data);
        }
    }

    afficheTopRated(data){

        for (let i = 0; i < this.swiperTotal; i++) {

            let uneSlide = document.querySelector(".template>div.swiper-slide").cloneNode(true);

            uneSlide.querySelector("h3").innerText = data[i].title;

            let uneImage = uneSlide.querySelector("img");
            uneImage.setAttribute("src", this.imgPath + "w" + this.posterSizes[3] + data[i].poster_path);

            uneSlide.querySelector(".lien-film").setAttribute("href", "fiche-film.html?id=" + data[i].id);


            document.querySelector("div.swiper-wrapper").appendChild(uneSlide);

        }

        var mySwiper = new Swiper('.swiper-container', {

            // Optional parameters
            direction: 'horizontal',
            loop: true,
            autoHeight: true,
            autoplay: {
                delay: 4000,
            },

            centeredSlides: 'true',

            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                // when window width is >= 625px
                426: {
                    slidesPerView: 3,
                    spaceBetween: 15
                },
                // when window width is >= 768px
                770: {
                    slidesPerView: 4,
                    spaceBetween: 20
                }
            },

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },



        })


    }

    requestPopular(){

        var xhr = new XMLHttpRequest();
        // xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.returnRequestPopular.bind(this));
        xhr.open("GET", this.baseURL +"movie/popular?page=1&language="+ this.lang +"&api_key="+ this.APIKey);
        xhr.send();
    }

    returnRequestPopular(e){

        let target = e.currentTarget; //XMLHttpRequest
        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            this.affichePopular(data);
        }
    }

    affichePopular(data){

        for (let i = 0; i < this.movieTotal; i++) {

            let unArticle = document.querySelector(".template>figure.article").cloneNode(true);


            unArticle.querySelector(".film-date").innerText = data[i].release_date;
            unArticle.querySelector(".film-rating .resultat").innerText = data[i].vote_average;
            unArticle.querySelector(".film-title").innerText = data[i].title;


            if (data[i].overview === ""){
                unArticle.querySelector(".description").innerText = "Désolé la version française dienne de ce film est indisponible.";
            } else{
                unArticle.querySelector(".description").innerText = data[i].overview;
            }

            let uneImage = unArticle.querySelector("img");
            uneImage.setAttribute("src", this.imgPath + "w" + this.posterSizes[3] + data[i].poster_path);

            unArticle.querySelector(".lien-film").setAttribute("href", "fiche-film.html?id=" + data[i].id);
            unArticle.querySelector(".film-footer .button").setAttribute("href", "fiche-film.html?id=" + data[i].id);


            document.querySelector("section.section-article").appendChild(unArticle);

        }
    }



    // Requête pour un film

    requestFilmInfos(idFilm){



        var xhr = new XMLHttpRequest();
        // xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.returnRequestFilmInfos.bind(this));
        xhr.open("GET", this.baseURL +"movie/"+ idFilm +"?language="+ this.lang +"&api_key="+ this.APIKey);
        xhr.send();

    }

    returnRequestFilmInfos(e){

        let target = e.currentTarget; //XMLHttpRequest
        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            this.afficheFilmInfos(data);
        }
    }

    afficheFilmInfos(data){



        document.querySelector("img.poster1").setAttribute("src", this.imgPath + "w" + this.posterSizes[4] + data.poster_path);
        document.querySelector("img.poster").setAttribute("src", this.imgPath + "w" + this.posterSizes[4] + data.poster_path);

        document.querySelector("h2").innerText = data.title;

        if (data.overview === ""){
            document.querySelector("p.description").innerText = "La version française canadienne du synopsis de ce film est indisponible.";
        } else{
            document.querySelector("p.description").innerText = data.overview;
        }
        document.querySelector("span.film-lang").innerText = data.original_language;
        document.querySelector(".film-rating .resultat").innerText = data.vote_average;


        document.querySelector("span.film-duree").innerText = data.runtime;
        document.querySelector("span.film-date").innerText = data.release_date;
        document.querySelector("span.film-revenue").innerText = data.revenue;

        document.querySelector("span.film-budget").innerText = data.budget;

    }



    // Requête des infos des acteurs

    requestCastInfos(idFilm){

        var xhr = new XMLHttpRequest();
        // xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.returnRequestCastInfos.bind(this));
        xhr.open("GET",  this.baseURL + "movie/"+ idFilm +"/credits?api_key="+ this.APIKey);
        xhr.send();
    }

    returnRequestCastInfos(e){

        let target = e.currentTarget; //XMLHttpRequest
        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).cast;

            this.afficheCastInfos(data);
        }

    }

    afficheCastInfos(data){


        for (let i = 0; i < this.castTotal; i++) {

            let unActeur = document.querySelector(".template>div.acteurs").cloneNode(true);

            unActeur.querySelector("img").setAttribute("src", this.imgPath + "w" + this.castSizes[1] + data[i].profile_path);
            unActeur.querySelector("p").innerText = data[i].name;

            document.querySelector("div.fiches-acteurs").appendChild(unActeur);
        }

    }


}


//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzY3JpcHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTUNvbnRlbnRMb2FkZWRcIiwgZnVuY3Rpb24gKGV2ZW50KSB7XHJcblxyXG4gICAgLy9VbiBjb21tZW50YWlyZVxyXG4gICAgY29uc29sZS5sb2coXCLDh2EgZm9uY3Rpb25uZSEhIVwiKTtcclxuXHJcbiAgICAvLyBCb3V0b24gcmV0b3VyIGVuIGhhdXQgZGUgbGEgcGFnZVxyXG4gICAgbXlidXR0b24gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI215QnRuXCIpO1xyXG5cclxuICAgIGZ1bmN0aW9uIHRvcEZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGRvY3VtZW50LmJvZHkuc2Nyb2xsVG9wID0gMDsgLy8gRm9yIFNhZmFyaVxyXG4gICAgICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3AgPSAwOyAvLyBGb3IgQ2hyb21lLCBGaXJlZm94LCBJRSBhbmQgT3BlcmFcclxuICAgIH1cclxuXHJcbiAgICBteWJ1dHRvbi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHRvcEZ1bmN0aW9uKTtcclxuXHJcbiAgICAvL0JEXHJcblxyXG4gICAgbGV0IGNvbm5leGlvbiA9IG5ldyBNb3ZpZURiKCk7XHJcblxyXG4gICAgY29uc29sZS5sb2coZG9jdW1lbnQubG9jYXRpb24pO1xyXG5cclxuICAgIGlmIChkb2N1bWVudC5sb2NhdGlvbi5wYXRobmFtZS5zZWFyY2goXCJmaWNoZS1maWxtLmh0bWxcIikgPiAwKXtcclxuXHJcbiAgICAgICAgbGV0IHBhcmFtcyA9ICggbmV3IFVSTChkb2N1bWVudC5sb2NhdGlvbikgKS5zZWFyY2hQYXJhbXM7XHJcblxyXG4gICAgICAgIGNvbm5leGlvbi5yZXF1ZXNGaWxtSW5mb3MoIHBhcmFtcy5nZXQoXCJpZFwiKSApO1xyXG4gICAgICAgIGNvbm5leGlvbi5yZXF1ZXN0Q2FzdEluZm9zKCBwYXJhbXMuZ2V0KFwiaWRcIikgKTtcclxuXHJcbiAgICB9XHJcbiAgICBlbHNle1xyXG4gICAgICAgIGNvbm5leGlvbi5yZXF1ZXN0UG9wdWxhcigpO1xyXG4gICAgICAgIGNvbm5leGlvbi5yZXF1ZXN0Tm93UGxheWluZygpO1xyXG4gICAgfVxyXG5cclxuXHJcblxyXG59KTtcclxuXHJcblxyXG5jbGFzcyBNb3ZpZURiIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuXHJcbiAgICAgICAgdGhpcy5BUElLZXkgPSBcIjZiYjIxYjkyYzJmYTU0OWY2NWUzMjczYmVlNGIzNmRjXCI7XHJcbiAgICAgICAgdGhpcy5sYW5nID0gXCJmci1DQVwiO1xyXG4gICAgICAgIHRoaXMuYmFzZVVSTCA9IFwiaHR0cHM6Ly9hcGkudGhlbW92aWVkYi5vcmcvMy9cIjtcclxuXHJcbiAgICAgICAgdGhpcy5pbWdQYXRoID0gXCJodHRwOi8vaW1hZ2UudG1kYi5vcmcvdC9wL1wiO1xyXG4gICAgICAgIHRoaXMucG9zdGVyU2l6ZXMgPSBbXCI5MlwiLCBcIjE1NFwiLCBcIjE4NVwiLCBcIjM0MlwiLCBcIjUwMFwiLCBcIjc4MFwiXTtcclxuICAgICAgICB0aGlzLmNhc3RTaXplcyA9IFtcIjQ1XCIsIFwiMTg1XCJdO1xyXG5cclxuICAgICAgICB0aGlzLm1vdmllVG90YWwgPSA2O1xyXG4gICAgICAgIHRoaXMuc3dpcGVyVG90YWwgPSA5O1xyXG4gICAgICAgIHRoaXMuY2FzdFRvdGFsID0gNjtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgcmVxdWVzdE5vd1BsYXlpbmcoKXtcclxuXHJcbiAgICAgICAgdmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG4gICAgICAgIC8vIHhoci53aXRoQ3JlZGVudGlhbHMgPSB0cnVlO1xyXG5cclxuICAgICAgICB4aHIuYWRkRXZlbnRMaXN0ZW5lcihcInJlYWR5c3RhdGVjaGFuZ2VcIiwgdGhpcy5yZXR1cm5SZXF1ZXN0Tm93UGxheWluZy5iaW5kKHRoaXMpKTtcclxuICAgICAgICB4aHIub3BlbihcIkdFVFwiLCB0aGlzLmJhc2VVUkwgK1wibW92aWUvbm93X3BsYXlpbmc/cGFnZT0xJmxhbmd1YWdlPVwiKyB0aGlzLmxhbmcgK1wiJmFwaV9rZXk9XCIrIHRoaXMuQVBJS2V5KTtcclxuICAgICAgICB4aHIuc2VuZCgpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVyblJlcXVlc3ROb3dQbGF5aW5nKGUpe1xyXG5cclxuICAgICAgICBsZXQgdGFyZ2V0ID0gZS5jdXJyZW50VGFyZ2V0OyAvL1hNTEh0dHBSZXF1ZXN0XHJcbiAgICAgICAgbGV0IGRhdGE7XHJcblxyXG4gICAgICAgIGlmICh0YXJnZXQucmVhZHlTdGF0ZSA9PT0gdGFyZ2V0LkRPTkUpIHtcclxuXHJcbiAgICAgICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKHRhcmdldC5yZXNwb25zZVRleHQpLnJlc3VsdHM7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmFmZmljaGVOb3dQbGF5aW5nKGRhdGEpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhZmZpY2hlTm93UGxheWluZyhkYXRhKXtcclxuXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnN3aXBlclRvdGFsOyBpKyspIHtcclxuXHJcbiAgICAgICAgICAgIGxldCB1bmVTbGlkZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIudGVtcGxhdGU+ZGl2LnN3aXBlci1zbGlkZVwiKS5jbG9uZU5vZGUodHJ1ZSk7XHJcblxyXG4gICAgICAgICAgICB1bmVTbGlkZS5xdWVyeVNlbGVjdG9yKFwiaDNcIikuaW5uZXJUZXh0ID0gZGF0YVtpXS50aXRsZTtcclxuXHJcbiAgICAgICAgICAgIGxldCB1bmVJbWFnZSA9IHVuZVNsaWRlLnF1ZXJ5U2VsZWN0b3IoXCJpbWdcIik7XHJcbiAgICAgICAgICAgIHVuZUltYWdlLnNldEF0dHJpYnV0ZShcInNyY1wiLCB0aGlzLmltZ1BhdGggKyBcIndcIiArIHRoaXMucG9zdGVyU2l6ZXNbM10gKyBkYXRhW2ldLnBvc3Rlcl9wYXRoKTtcclxuXHJcbiAgICAgICAgICAgIHVuZVNsaWRlLnF1ZXJ5U2VsZWN0b3IoXCIubGllbi1maWxtXCIpLnNldEF0dHJpYnV0ZShcImhyZWZcIiwgXCJmaWNoZS1maWxtLmh0bWw/aWQ9XCIgKyBkYXRhW2ldLmlkKTtcclxuXHJcblxyXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiZGl2LnN3aXBlci13cmFwcGVyXCIpLmFwcGVuZENoaWxkKHVuZVNsaWRlKTtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2YXIgbXlTd2lwZXIgPSBuZXcgU3dpcGVyKCcuc3dpcGVyLWNvbnRhaW5lcicsIHtcclxuXHJcbiAgICAgICAgICAgIC8vIE9wdGlvbmFsIHBhcmFtZXRlcnNcclxuICAgICAgICAgICAgZGlyZWN0aW9uOiAnaG9yaXpvbnRhbCcsXHJcbiAgICAgICAgICAgIGxvb3A6IHRydWUsXHJcbiAgICAgICAgICAgIGF1dG9IZWlnaHQ6IHRydWUsXHJcbiAgICAgICAgICAgIGF1dG9wbGF5OiB7XHJcbiAgICAgICAgICAgICAgICBkZWxheTogNDAwMCxcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIC8vIHNsaWRlc1BlclZpZXc6IDMsXHJcbiAgICAgICAgICAgIC8vIHNwYWNlQmV0d2VlbjogMjAsXHJcbiAgICAgICAgICAgIGNlbnRlcmVkU2xpZGVzOiAndHJ1ZScsXHJcblxyXG4gICAgICAgICAgICBicmVha3BvaW50czoge1xyXG4gICAgICAgICAgICAgICAgLy8gd2hlbiB3aW5kb3cgd2lkdGggaXMgPj0gMzIwcHhcclxuICAgICAgICAgICAgICAgIDMyMDoge1xyXG4gICAgICAgICAgICAgICAgICAgIHNsaWRlc1BlclZpZXc6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgc3BhY2VCZXR3ZWVuOiAxMFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIC8vIHdoZW4gd2luZG93IHdpZHRoIGlzID49IDYyNXB4XHJcbiAgICAgICAgICAgICAgICA0MjY6IHtcclxuICAgICAgICAgICAgICAgICAgICBzbGlkZXNQZXJWaWV3OiAzLFxyXG4gICAgICAgICAgICAgICAgICAgIHNwYWNlQmV0d2VlbjogMTVcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAvLyB3aGVuIHdpbmRvdyB3aWR0aCBpcyA+PSA3NjhweFxyXG4gICAgICAgICAgICAgICAgNzcwOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVzUGVyVmlldzogNCxcclxuICAgICAgICAgICAgICAgICAgICBzcGFjZUJldHdlZW46IDIwXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICAvLyBJZiB3ZSBuZWVkIHBhZ2luYXRpb25cclxuICAgICAgICAgICAgcGFnaW5hdGlvbjoge1xyXG4gICAgICAgICAgICAgICAgZWw6ICcuc3dpcGVyLXBhZ2luYXRpb24nLFxyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgLy8gTmF2aWdhdGlvbiBhcnJvd3NcclxuICAgICAgICAgICAgbmF2aWdhdGlvbjoge1xyXG4gICAgICAgICAgICAgICAgbmV4dEVsOiAnLnN3aXBlci1idXR0b24tbmV4dCcsXHJcbiAgICAgICAgICAgICAgICBwcmV2RWw6ICcuc3dpcGVyLWJ1dHRvbi1wcmV2JyxcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIC8vIEFuZCBpZiB3ZSBuZWVkIHNjcm9sbGJhclxyXG4gICAgICAgICAgICBzY3JvbGxiYXI6IHtcclxuICAgICAgICAgICAgICAgIGhpZGU6ICcuc3dpcGVyLXNjcm9sbGJhcicsXHJcbiAgICAgICAgICAgICAgICBkcmFnZ2FibGU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBkcmFnU2l6ZTogJ2F1dG8nLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgcmVxdWVzdFBvcHVsYXIoKXtcclxuXHJcbiAgICAgICAgdmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG4gICAgICAgIC8vIHhoci53aXRoQ3JlZGVudGlhbHMgPSB0cnVlO1xyXG5cclxuICAgICAgICB4aHIuYWRkRXZlbnRMaXN0ZW5lcihcInJlYWR5c3RhdGVjaGFuZ2VcIiwgdGhpcy5yZXR1cm5SZXF1ZXN0UG9wdWxhci5iaW5kKHRoaXMpKTtcclxuICAgICAgICB4aHIub3BlbihcIkdFVFwiLCB0aGlzLmJhc2VVUkwgK1wibW92aWUvcG9wdWxhcj9wYWdlPTEmbGFuZ3VhZ2U9XCIrIHRoaXMubGFuZyArXCImYXBpX2tleT1cIisgdGhpcy5BUElLZXkpO1xyXG4gICAgICAgIHhoci5zZW5kKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuUmVxdWVzdFBvcHVsYXIoZSl7XHJcblxyXG4gICAgICAgIGxldCB0YXJnZXQgPSBlLmN1cnJlbnRUYXJnZXQ7IC8vWE1MSHR0cFJlcXVlc3RcclxuICAgICAgICBsZXQgZGF0YTtcclxuXHJcbiAgICAgICAgaWYgKHRhcmdldC5yZWFkeVN0YXRlID09PSB0YXJnZXQuRE9ORSkge1xyXG5cclxuICAgICAgICAgICAgZGF0YSA9IEpTT04ucGFyc2UodGFyZ2V0LnJlc3BvbnNlVGV4dCkucmVzdWx0cztcclxuXHJcbiAgICAgICAgICAgIHRoaXMuYWZmaWNoZVBvcHVsYXIoZGF0YSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGFmZmljaGVQb3B1bGFyKGRhdGEpe1xyXG5cclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMubW92aWVUb3RhbDsgaSsrKSB7XHJcblxyXG4gICAgICAgICAgICBsZXQgdW5BcnRpY2xlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi50ZW1wbGF0ZT5maWd1cmUuYXJ0aWNsZVwiKS5jbG9uZU5vZGUodHJ1ZSk7XHJcblxyXG4gICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcIi5maWxtLXRpdGxlXCIpLmlubmVyVGV4dCA9IGRhdGFbaV0udGl0bGU7XHJcbiAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiLmZpbG0tZGF0ZVwiKS5pbm5lclRleHQgPSBkYXRhW2ldLnJlbGVhc2VfZGF0ZTtcclxuICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCIuZmlsbS1yYXRpbmcgLm5vdGVcIikuaW5uZXJUZXh0ID0gZGF0YVtpXS52b3RlX2F2ZXJhZ2U7XHJcblxyXG4gICAgICAgICAgICBpZiAoZGF0YVtpXS5vdmVydmlldyA9PT0gXCJcIil7XHJcbiAgICAgICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcIi5kZXNjcmlwdGlvblwiKS5pbm5lclRleHQgPSBcIkxhIHZlcnNpb24gZnJhbsOnYWlzZSBjYW5hZGllbm5lIGR1IHN5bm9wc2lzIGRlIGNlIGZpbG0gZXN0IGluZGlzcG9uaWJsZS5cIjtcclxuICAgICAgICAgICAgfSBlbHNle1xyXG4gICAgICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCIuZGVzY3JpcHRpb25cIikuaW5uZXJUZXh0ID0gZGF0YVtpXS5vdmVydmlldztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgbGV0IHVuZUltYWdlID0gdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJpbWdcIik7XHJcbiAgICAgICAgICAgIHVuZUltYWdlLnNldEF0dHJpYnV0ZShcInNyY1wiLCB0aGlzLmltZ1BhdGggKyBcIndcIiArIHRoaXMucG9zdGVyU2l6ZXNbM10gKyBkYXRhW2ldLnBvc3Rlcl9wYXRoKTtcclxuXHJcbiAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiLmxpZW4tZmlsbVwiKS5zZXRBdHRyaWJ1dGUoXCJocmVmXCIsIFwiZmljaGUtZmlsbS5odG1sP2lkPVwiICsgZGF0YVtpXS5pZCk7XHJcbiAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiLmZpbG0tZm9vdGVyIC5jdGFcIikuc2V0QXR0cmlidXRlKFwiaHJlZlwiLCBcImZpY2hlLWZpbG0uaHRtbD9pZD1cIiArIGRhdGFbaV0uaWQpO1xyXG5cclxuXHJcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJzZWN0aW9uLnNlY3Rpb24tYXJ0aWNsZVwiKS5hcHBlbmRDaGlsZCh1bkFydGljbGUpO1xyXG5cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG5cclxuXHJcbiAgICAvLyBSZXF1w6p0ZSBwb3VyIHVuIGZpbG1cclxuXHJcbiAgICByZXF1ZXNGaWxtSW5mb3MoaWRGaWxtKXtcclxuXHJcbiAgICAgICAgY29uc29sZS5sb2coXCJMZSBpZDogXCIgKyBpZEZpbG0pO1xyXG5cclxuICAgICAgICB2YXIgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XHJcbiAgICAgICAgLy8geGhyLndpdGhDcmVkZW50aWFscyA9IHRydWU7XHJcblxyXG4gICAgICAgIHhoci5hZGRFdmVudExpc3RlbmVyKFwicmVhZHlzdGF0ZWNoYW5nZVwiLCB0aGlzLnJldHVyblJlcXVlc3RGaWxtSW5mb3MuYmluZCh0aGlzKSk7XHJcbiAgICAgICAgeGhyLm9wZW4oXCJHRVRcIiwgdGhpcy5iYXNlVVJMICtcIm1vdmllL1wiKyBpZEZpbG0gK1wiP2xhbmd1YWdlPVwiKyB0aGlzLmxhbmcgK1wiJmFwaV9rZXk9XCIrIHRoaXMuQVBJS2V5KTtcclxuICAgICAgICB4aHIuc2VuZCgpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm5SZXF1ZXN0RmlsbUluZm9zKGUpe1xyXG5cclxuICAgICAgICBsZXQgdGFyZ2V0ID0gZS5jdXJyZW50VGFyZ2V0OyAvL1hNTEh0dHBSZXF1ZXN0XHJcbiAgICAgICAgbGV0IGRhdGE7XHJcblxyXG4gICAgICAgIGlmICh0YXJnZXQucmVhZHlTdGF0ZSA9PT0gdGFyZ2V0LkRPTkUpIHtcclxuXHJcbiAgICAgICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKHRhcmdldC5yZXNwb25zZVRleHQpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5hZmZpY2hlRmlsbUluZm9zKGRhdGEpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhZmZpY2hlRmlsbUluZm9zKGRhdGEpe1xyXG5cclxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhLnRpdGxlKTtcclxuXHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcImltZy5wb3N0ZXJcIikuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRoaXMuaW1nUGF0aCArIFwid1wiICsgdGhpcy5wb3N0ZXJTaXplc1s0XSArIGRhdGEucG9zdGVyX3BhdGgpO1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJoMlwiKS5pbm5lclRleHQgPSBkYXRhLnRpdGxlO1xyXG5cclxuICAgICAgICBpZiAoZGF0YS5vdmVydmlldyA9PT0gXCJcIil7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJwLmRlc2NyaXB0aW9uXCIpLmlubmVyVGV4dCA9IFwiTGEgdmVyc2lvbiBmcmFuw6dhaXNlIGNhbmFkaWVubmUgZHUgc3lub3BzaXMgZGUgY2UgZmlsbSBlc3QgaW5kaXNwb25pYmxlLlwiO1xyXG4gICAgICAgIH0gZWxzZXtcclxuICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInAuZGVzY3JpcHRpb25cIikuaW5uZXJUZXh0ID0gZGF0YS5vdmVydmlldztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuZmlsbS1yYXRpbmcgLm5vdGVcIikuaW5uZXJUZXh0ID0gZGF0YS52b3RlX2F2ZXJhZ2U7XHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInNwYW4uZmlsbS1kYXRlXCIpLmlubmVyVGV4dCA9IGRhdGEucmVsZWFzZV9kYXRlO1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJzcGFuLmZpbG0tbGFuZ1wiKS5pbm5lclRleHQgPSBkYXRhLm9yaWdpbmFsX2xhbmd1YWdlO1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJzcGFuLmZpbG0tZHVyZWVcIikuaW5uZXJUZXh0ID0gZGF0YS5ydW50aW1lO1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJzcGFuLmZpbG0tYnVkZ2V0XCIpLmlubmVyVGV4dCA9IGRhdGEuYnVkZ2V0O1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJzcGFuLmZpbG0tcmV2ZW51ZVwiKS5pbm5lclRleHQgPSBkYXRhLnJldmVudWU7XHJcblxyXG5cclxuICAgIH1cclxuXHJcblxyXG5cclxuICAgIC8vIFJlcXXDqnRlIGRlcyBpbmZvcyBkZXMgYWN0ZXVyc1xyXG5cclxuICAgIHJlcXVlc3RDYXN0SW5mb3MoaWRGaWxtKXtcclxuXHJcbiAgICAgICAgdmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG4gICAgICAgIC8vIHhoci53aXRoQ3JlZGVudGlhbHMgPSB0cnVlO1xyXG5cclxuICAgICAgICB4aHIuYWRkRXZlbnRMaXN0ZW5lcihcInJlYWR5c3RhdGVjaGFuZ2VcIiwgdGhpcy5yZXR1cm5SZXF1ZXN0Q2FzdEluZm9zLmJpbmQodGhpcykpO1xyXG4gICAgICAgIHhoci5vcGVuKFwiR0VUXCIsICB0aGlzLmJhc2VVUkwgKyBcIm1vdmllL1wiKyBpZEZpbG0gK1wiL2NyZWRpdHM/YXBpX2tleT1cIisgdGhpcy5BUElLZXkpO1xyXG4gICAgICAgIHhoci5zZW5kKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuUmVxdWVzdENhc3RJbmZvcyhlKXtcclxuXHJcbiAgICAgICAgbGV0IHRhcmdldCA9IGUuY3VycmVudFRhcmdldDsgLy9YTUxIdHRwUmVxdWVzdFxyXG4gICAgICAgIGxldCBkYXRhO1xyXG5cclxuICAgICAgICBpZiAodGFyZ2V0LnJlYWR5U3RhdGUgPT09IHRhcmdldC5ET05FKSB7XHJcblxyXG4gICAgICAgICAgICBkYXRhID0gSlNPTi5wYXJzZSh0YXJnZXQucmVzcG9uc2VUZXh0KS5jYXN0O1xyXG5cclxuICAgICAgICAgICAgdGhpcy5hZmZpY2hlQ2FzdEluZm9zKGRhdGEpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcblxyXG4gICAgYWZmaWNoZUNhc3RJbmZvcyhkYXRhKXtcclxuXHJcblxyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5jYXN0VG90YWw7IGkrKykge1xyXG5cclxuICAgICAgICAgICAgbGV0IHVuQWN0ZXVyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi50ZW1wbGF0ZT5kaXYuYWN0ZXVyXCIpLmNsb25lTm9kZSh0cnVlKTtcclxuXHJcbiAgICAgICAgICAgIHVuQWN0ZXVyLnF1ZXJ5U2VsZWN0b3IoXCJpbWdcIikuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRoaXMuaW1nUGF0aCArIFwid1wiICsgdGhpcy5jYXN0U2l6ZXNbMV0gKyBkYXRhW2ldLnByb2ZpbGVfcGF0aCk7XHJcbiAgICAgICAgICAgIHVuQWN0ZXVyLnF1ZXJ5U2VsZWN0b3IoXCJwXCIpLmlubmVyVGV4dCA9IGRhdGFbaV0ubmFtZTtcclxuXHJcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJkaXYuZmljaGVzLWFjdGV1cnNcIikuYXBwZW5kQ2hpbGQodW5BY3RldXIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcblxyXG5cclxufVxyXG5cclxuIl0sImZpbGUiOiJzY3JpcHQuanMifQ==
